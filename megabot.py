import time, pyautogui
import numpy as np
import csv
#pyautogui.PAUSE = 0
pyautogui.FAILSAFE = True

#find the current mouse location (so we know where to set the search regions for the hp bar images later)
#x, y = pyautogui.position()

#this clicks on the emulator window to activate it before pressing buttons
pyautogui.click(1485, 13, button='left')
#pyautogui.click(1180, 233, button='left')

#This part looks for an image on screen and prints its location, in this case the boss' health
#print(pyautogui.locateOnScreen('C:\megabot\woodmanhp.png'))
#print(x)
#print(y)

#Generation Count
gen = 0

#function to lift all currently pressed buttons
def allkeysup():
    pyautogui.keyUp('w')
    pyautogui.keyUp('s')
    pyautogui.keyUp('a')
    pyautogui.keyUp('d')
    pyautogui.keyUp('x')
    pyautogui.keyUp('z')

#shoots megaman's gun, this used to have more to it, it cna probably be removed    
def shoot():
    pyautogui.keyDown('z')
    pyautogui.keyUp('z')

#makes megaman jump, need to refine this so he cna jump at varying heights	
def jump(height):
    pyautogui.keyDown('x')
    if (height == 0):
        pyautogui.keyUp('x')

#create a random list of numbers 0-5, this is a 'bot'		
def createactions():
    numbers = []
    for x in range (0, 150):
        numbers.append(np.random.randint(0, 6))
    return numbers

#create 190 bots to start gen 0
def createbots():
    bots = []
    for y in range (0, 190):
        bots.append(createactions())
    return bots

#takes the list of numbers for the bot and presses buttons based on the number
def megamancontrol(numbers):
    allkeysup()
    for number in numbers:
        if number == 0:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('a')
        elif number == 1:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('d')
        elif number == 2:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('w')
        elif number == 3:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('s')
        elif number == 4:
            healthcheck(bot)
            jump(1)
        elif number == 5:
            healthcheck(bot)
            shoot()

#count the bots of this gen            
botnumber = 0

#runs the bot through the megaman control function (it runs the bot)           
def play(bot, botnumber):
    print(bot)
    print('Generation '+str(gen))
    print('Bot Number '+str(botnumber)+' Running')
    pyautogui.keyDown('p')
    pyautogui.keyUp('p')
    megamancontrol(bot)
        
gameover = 0 
gameoverlist = []
results = []
#check megaman's hp and the boss' hp, then subtract bosshp from megaman hp, min score -28 max 28
def healthcheck(bot):
        ossbar = list(pyautogui.locateAllOnScreen('c:\megabot\ossbar1.png', region=(1178, 101, 3, 168)))
        ossbar1 = list(pyautogui.locateAllOnScreen('c:\megabot\ossbar1.png', region=(1131, 101, 3, 167)))
        megamanhp = list(pyautogui.locateAllOnScreen('c:\megabot\hpbar.png', region=(1131, 101, 24, 167)))
        woodmanhp = list(pyautogui.locateAllOnScreen('c:\megabot\woodhpbar.png', region=(1179, 101, 24, 168)))
        mmhp = len(megamanhp)
        wmhp = len(woodmanhp)
        fitness = mmhp - wmhp
		#there are a series of checks to make sure that if either hp == 0, it is a legitimate death and not just the hp bars flickering on/off, if it cant determine it, it gives an invalid fitness score and the bot is killed
        if (mmhp == 0 or wmhp == 0 and len(ossbar) >= 1 and len(ossbar1) >= 1):
            gameoverlist.append([mmhp, wmhp, fitness])
            if (len(gameoverlist) == 4):
                gameoverlist.pop(0)
                finalscore = gameoverlist[1]
                fitnessScore = finalscore[2]
                if (all_same(gameoverlist) == True):
                    if (gameoverlist[1] == [0,0,0]):
                            results.append([bot, -100])
                            print('Score: Invalid')
                            raise exception    
                    else:
                        results.append([bot, fitnessScore])
                        print('Score: '+str(fitnessScore))
                        raise exception

def all_same(items):
    return all(x == items[0] for x in items)

#this old breeding function only took the top 10
'''
def top10(results, gen):
    print('Results are in!')
    results.sort(key = lambda row: row[1])
    top10 = []
    top10 = results[-10:]
    with open("Top 10 of Each Generation.csv", "a+") as f:
        writer = csv.writer(f)
        writer.writerow(['Generation', ''+str(gen)+''])
        writer.writerows(top10)
    return top10
'''

#New breeding function records top 10 for each gen, but actual breeding is based on weighted chance up to 10 breeders, fittest go first. Now every bot can technically breed if the most fit fail to do so.
#This adds more variety and prevents evolution from getting stagnant
def breed(results, gen):
    breeders = []
    selected = 0
    print('Results are in!')
    results.sort(key = lambda row: row[1])
    top10 = []
    top10 = results[-10:]
    spins = 0
    breedloops = 0
    with open("Top 10 of Each Generation.csv", "a+") as f:
        writer = csv.writer(f)
        writer.writerow(['Generation', ''+str(gen)+''])
        writer.writerows(top10)
    while selected <= 10:
        if spins >= 1000000:
            bots = createbots()
            selected += 11
            return bots
        fitness = results[breedloops][1]
        if fitness == 28:
            #write a program stopping line here
            print('winner')
            #break
        print('spinning')
        roulette = (np.random.randint(-30, 30))
        spins +=1
        print('Spin Number ' + str(spins))
        print(roulette)
        print('Breeders found: ' +str(len(breeders)))
        print('Selected: ' + str(selected))
        if fitness >= roulette:
            if results[breedloops] not in breeders:
                print('breeder found')
                breeders.append(results[breedloops])
                selected += 1
        breedloops += 1
        if breedloops == 190:
            breedloops = 0
    bots = []
    i = 0
    x = 0
    count = 0
#for bot in breeders:
    for x in range (0,10):
        for i in range (0,10):
            a = []
            b = []
            c = []
            d = []
            s = np.random.randint(1, 149)
            e = s-150
            mutation = 0
            if x == i:
                print('skip')
            else:
                #print('parent 1')
                a = (a + breeders[x][0])
                #print('parent 2')
                b = (b + breeders[i][0])                
                #print('part of parent 1')
                c = (c + a[0:s])
                #print('completed with parent 2')
                c = (c + b[e:])
                #print('Other part of parent 1')
                d = (d + b[0:s])
                #print('Other part completed with parent 2')
                d = (d + a[e:])
                #print('Appending to bots list')
                mutation = np.random.randint(1, 1000)
                if mutation == 1:
                    c[np.random.randint(0, 149)] = np.random.randint(0, 6)
                mutation = np.random.randint(1, 1000)
                if mutation == 1:
                    d[np.random.randint(0, 149)] = np.random.randint(0, 6)
                bots.append(c)
                bots.append(d)
                count += 2
                print('2 bots created, '+ str(count) +' Total')
        #bots.append(a) #this adds the top 10 from the previous generation, that causes a lot of problems
    #add 10 random bots each gen to avoid getting stale
    for y in range (0, 10):
        bots.append(createactions())
    print('THIS GENERATION CONTAINS ' + str(len(bots)) + 'BOTS')
    print('THIS GENERATION CONTAINS ' + str(len(bots)) + 'BOTS')
    print('THIS GENERATION CONTAINS ' + str(len(bots)) + 'BOTS')
    return bots   

#this is part of the old top10 breed function that was replaced
'''
def breed(top10):
        bots = []
        i = 0
        x = 0
        count = 0
        print('breeding')
        for x in range (0,9):
            for i in range (0,9):
                a = []
                b = []
                c = []
                d = []
                s = np.random.randint(1, 149)
                e = s-150
                mutation = 0
                if x == i:
                    print('skip')
                else:
                    print('parent 1')
                    a = (a + top10[x][0])
                    print('parent 2')
                    b = (b + top10[i][0])                
                    print('part of parent 1')
                    c = (c + a[0:s])
                    print('completed with parent 2')
                    c = (c + b[e:])
                    print('Other part of parent 1')
                    d = (d + b[0:s])
                    print('Other part completed with parent 2')
                    d = (d + a[e:])
                    print('Appending to bots list')
                    mutation = np.random.randint(1, 1000)
                    if mutation == 1:
                        c[np.random.randint(0, 149)] = np.random.randint(0, 6)
                    mutation = np.random.randint(1, 1000)
                    if mutation == 1:
                        d[np.random.randint(0, 149)] = np.random.randint(0, 6)
                    bots.append(c)
                    bots.append(d)
                    count += 2
                    print('2 bots created, '+ str(count) +' Total')
            bots.append(a)
        #add 10 random bots each gen to avoid getting stale
        for y in range (0, 10):
            bots.append(createactions())
        return bots
'''

#create the first 190 bots at random
print('The First Generation is being created!')
bots = createbots()
print('The First Generation has been created!')
botcount = 0 
for bot in bots:
    botnumber += 1
    try:  
         play(bot, botnumber)
    except:
        pass

#clear the bot list so we can repopulate it
bots = []
#repopulate bot list with breed functions
bots = breed(results,gen)
#clear previous gen's results before next breed
results = []

#run for 1000 generations then stop
for g in range (0,1000):
    gen += 1
    botnumber = 0
    for bot in bots:
        botnumber += 1
        try:  
            play(bot, botnumber)
        except:
            pass
    results.sort(key = lambda row: row[1])
    bots = []
    bots = breed(results,gen)
    results = []
        

import time, pyautogui
import numpy as np
import csv

pyautogui.FAILSAFE = True
pyautogui.click(1485, 13, button='left')
gen = 0

def allkeysup():
    pyautogui.keyUp('w')
    pyautogui.keyUp('s')
    pyautogui.keyUp('a')
    pyautogui.keyUp('d')
    pyautogui.keyUp('x')
    pyautogui.keyUp('z')
    
def shoot():
    pyautogui.keyDown('z')
    pyautogui.keyUp('z')
        
def jump(height):
    pyautogui.keyDown('x')
    if (height == 0):
        pyautogui.keyUp('x')

def createactions():
    numbers = []
    for x in range (0, 150):
        numbers.append(np.random.randint(0, 6))
    return numbers

def createbots():
    bots = []
    for y in range (0, 190):
        bots.append(createactions())
    return bots

def megamancontrol(numbers):
    allkeysup()
    for number in numbers:
        if number == 0:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('a')
        elif number == 1:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('d')
        elif number == 2:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('w')
        elif number == 3:
            healthcheck(bot)
            allkeysup()
            pyautogui.keyDown('s')
        elif number == 4:
            healthcheck(bot)
            jump(1)
        elif number == 5:
            healthcheck(bot)
            shoot()
            
botnumber = 0            
def play(bot, botnumber):
    print(bot)
    print('Generation '+str(gen))
    print('Bot Number '+str(botnumber)+' Running')
    pyautogui.keyDown('p')
    pyautogui.keyUp('p')
    megamancontrol(bot)
        
gameover = 0 
gameoverlist = []
results = []
def healthcheck(bot):
        ossbar = list(pyautogui.locateAllOnScreen('c:\megabot\ossbar1.png', region=(1178, 101, 3, 168)))
        ossbar1 = list(pyautogui.locateAllOnScreen('c:\megabot\ossbar1.png', region=(1131, 101, 3, 167)))
        megamanhp = list(pyautogui.locateAllOnScreen('c:\megabot\hpbar.png', region=(1131, 101, 24, 167)))
        woodmanhp = list(pyautogui.locateAllOnScreen('c:\megabot\woodhpbar.png', region=(1179, 101, 24, 168)))
        mmhp = len(megamanhp)
        wmhp = len(woodmanhp)
        fitness = mmhp - wmhp
        if (mmhp == 0 or wmhp == 0 and len(ossbar) >= 1 and len(ossbar1) >= 1):
            gameoverlist.append([mmhp, wmhp, fitness])
            if (len(gameoverlist) == 4):
                gameoverlist.pop(0)
                finalscore = gameoverlist[1]
                fitnessScore = finalscore[2]
                if (all_same(gameoverlist) == True):
                    if (gameoverlist[1] == [0,0,0]):
                            results.append([bot, -100])
                            print('Score: Invalid')
                            raise exception    
                    else:
                        results.append([bot, fitnessScore])
                        print('Score: '+str(fitnessScore))
                        raise exception

def all_same(items):
    return all(x == items[0] for x in items)

def breed(results, gen):
    breeders = []
    selected = 0
    print('Results are in!')
    results.sort(key = lambda row: row[1])
    top10 = []
    top10 = results[-10:]
    spins = 0
    breedloops = 0
    with open("Top 10 of Each Generation.csv", "a+") as f:
        writer = csv.writer(f)
        writer.writerow(['Generation', ''+str(gen)+''])
        writer.writerows(top10)
    while selected <= 10:
        if spins >= 1000000:
            bots = createbots()
            selected += 11
            return bots
        fitness = results[breedloops][1]
        if fitness == 28:
            #write a program stopping line here
            print('winner')
            #break
        print('spinning')
        roulette = (np.random.randint(-30, 30))
        spins +=1
        print('Spin Number ' + str(spins))
        print(roulette)
        print('Breeders found: ' +str(len(breeders)))
        print('Selected: ' + str(selected))
        if fitness >= roulette:
            if results[breedloops] not in breeders:
                print('breeder found')
                breeders.append(results[breedloops])
                selected += 1
        breedloops += 1
        if breedloops == 190:
            breedloops = 0
    bots = []
    i = 0
    x = 0
    count = 0
    for x in range (0,10):
        for i in range (0,10):
            a = []
            b = []
            c = []
            d = []
            s = np.random.randint(1, 149)
            e = s-150
            mutation = 0
            if x == i:
                print('skip')
            else:
                a = (a + breeders[x][0])
                b = (b + breeders[i][0])                
                c = (c + a[0:s])
                c = (c + b[e:])
                d = (d + b[0:s])
                d = (d + a[e:])
                mutation = np.random.randint(1, 1000)
                if mutation == 1:
                    c[np.random.randint(0, 149)] = np.random.randint(0, 6)
                mutation = np.random.randint(1, 1000)
                if mutation == 1:
                    d[np.random.randint(0, 149)] = np.random.randint(0, 6)
                bots.append(c)
                bots.append(d)
                count += 2
                print('2 bots created, '+ str(count) +' Total')
    for y in range (0, 10):
        bots.append(createactions())
    print('THIS GENERATION CONTAINS ' + str(len(bots)) + 'BOTS')
    print('THIS GENERATION CONTAINS ' + str(len(bots)) + 'BOTS')
    print('THIS GENERATION CONTAINS ' + str(len(bots)) + 'BOTS')
    return bots   

print('The First Generation is being created!')
bots = createbots()
print('The First Generation has been created!')
botcount = 0 
for bot in bots:
    botnumber += 1
    try:  
         play(bot, botnumber)
    except:
        pass

bots = []
bots = breed(results,gen)
results = []

for g in range (0,1000):
    gen += 1
    botnumber = 0
    for bot in bots:
        botnumber += 1
        try:  
            play(bot, botnumber)
        except:
            pass
    results.sort(key = lambda row: row[1])
    bots = []
    bots = breed(results,gen)
    results = []
        
